package com.test.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.java.model.User;
import com.test.java.repository.UserRepository;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public Boolean saveDataUser(User user){
      
        userRepository.insertDataUser(user.getId(), user.getName(), user.getAge());

        return true;
    }

    public List<User> getAllDatauser() {
		return userRepository.findAll();
	}
    
}
