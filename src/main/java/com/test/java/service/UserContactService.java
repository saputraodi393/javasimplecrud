package com.test.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.java.model.UserContact;
import com.test.java.repository.UserContactRepository;

@Service
public class UserContactService {

    @Autowired
    UserContactRepository userContactRepository;


    public List<UserContact> getAllusercontact() {
		return userContactRepository.getUserData();
	}

    public Boolean saveUserContact(UserContact userContact){
         userContactRepository.insertDataUserContact(userContact.getId(), userContact.getAddress());
         return true;
    }
    
}
