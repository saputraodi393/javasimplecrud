package com.test.java.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.java.model.User;
import com.test.java.model.UserContact;
import com.test.java.model.resphonse.MessageResponse;
import com.test.java.service.UserContactService;
import com.test.java.service.UserService;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserContactService userContactService;

    @PostMapping("/addUser")
    public ResponseEntity<MessageResponse> saveUser(@RequestBody User user){
        userService.saveDataUser(user);
        return ResponseEntity.ok(new MessageResponse("succsess create data user" + user.getId()));
    }

    @PostMapping("/addUserContact")
    public ResponseEntity<MessageResponse> saveUser(@RequestBody UserContact usercContact){
        userContactService.saveUserContact(usercContact);
        return ResponseEntity.ok(new MessageResponse("succsess create data user" + usercContact.getId()));
    }
    
    @GetMapping("/searchAllUser")
	public List<User> customerStats() {
        return  userService.getAllDatauser();
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<UserContact>> getAll(){
        return new ResponseEntity<List<UserContact>>(userContactService.getAllusercontact(),HttpStatus.OK);
    }

}
