package com.test.java.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "tb_m_user_contact")
@Setter
@Getter
@NoArgsConstructor
public class UserContact implements Serializable{

  
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private String id;

    @Column(name = "ADDRESS")
    private String address;

    @OneToOne
    @JoinColumn(name  = "id_user")
    private User user;

    
}
