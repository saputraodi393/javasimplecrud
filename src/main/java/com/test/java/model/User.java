package com.test.java.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tb_m_user")
@Setter
@Getter
public class User implements Serializable {

    @Id
    @GeneratedValue  
    @Column(name = "id_user")
    private String id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "AGE")
    private int age;

    
}
