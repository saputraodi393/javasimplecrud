package com.test.java.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.test.java.model.User;


@Repository
public interface UserRepository extends JpaRepository<User,Long> {


    @Modifying
    @Transactional
    @Query(value = "insert into tb_m_user (id_user, name, age) VALUES (?1, ?2, ?3)", nativeQuery = true)
    void insertDataUser(String id,String name,int age);
    
}
    