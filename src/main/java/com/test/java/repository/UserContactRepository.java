package com.test.java.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.test.java.model.UserContact;

@Repository
public interface UserContactRepository extends JpaRepository<UserContact,String> {


    @Query(value = "select d.id,e.id_user,e.name,e.age,d.address FROM tb_m_user_contact d INNER JOIN tb_m_user e on  e.id_user = d.id", nativeQuery = true)
	List<UserContact> getUserData();

    @Modifying
    @Transactional
    @Query(value = "insert into tb_m_user_contact (id, address) VALUES (?1, ?2)", nativeQuery = true)
    void insertDataUserContact(String id,String address);
}
